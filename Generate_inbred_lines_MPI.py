##############################################################################
#
#  MPI version
#
#  Workshare the generation and selection of hybrids.
#
##############################################################################

import argparse
import os
import time
import pickle
import sys
import logging
import mpi4py.MPI
import numpy as np
from heterosis.Model_Data import *
from heterosis.core import *
import heterosis.extract as extract
import heterosis.MPIHelper as MPIHelper

import ZODB.FileStorage
import ZODB.DB
import transaction


save_initial_parents = True
selection_methods_avail = ['F', 'F-TFb','F-GDb', 'F-TC', 'random',
                           'TC-TF', 'TF-TC', 'GD', 'TF-Fb' , 'GD-Fb']

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s %(name)s %(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger("Generate_inbred_lines")

def get_args():
    desc = '''
    Simulate selection pressure over multiple generations
    '''

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-s', '--size',
                        help='Number of selected individuals between \
                        generations', required=True)
    parser.add_argument('-p', '--parents', help='Number of initial parents',
                        required=True)
    parser.add_argument('-r', '--parent_directory',
                        help='Directory containing the initial models (pickles)',
                        required=True)
    parser.add_argument('-i', '--reference_model',
                        help='Reference model (pickle)', required=True)
    parser.add_argument('-x', '--biomass_reaction_id',
                        help='SBML ID of the biomass reaction to use',
                        required=True)
    parser.add_argument('-n', '--n_gen', help='Number of generations',
                        required=True)
    parser.add_argument('-g', '--start_gen', help='Starting generation number',
                        required=False, default=1)
    parser.add_argument('-t', '--t_hybrids',
                        help='The quantity of generated hybrids',
                        required=True)
    parser.add_argument('-o', '--output_file', help='output file (db)',
                        required=True)
    parser.add_argument('-z', '--random_seed',
                        help='Global RNG seed (integer >0)', required=True)
    parser.add_argument('-m', '--selection_method',
                        help="Define the selection method ('best'[default]|'random')",
                        default='best', required=True)
    parser.add_argument('-a', '--save_all', help='Save all the individuals',
                        required=False, action='store_true')
    parser.add_argument('-d', '--debug', help='debug mode (verbose)',
                        required=False, action='store_true')
    return parser.parse_args()


def get_initial_parents_single(args):
    '''
    Load or generate a collection of initial parents (seeds)
    :param args: main ArgumentParser object
    :param biomass_reaction_id: objective function ID
    :return: the reference model
             the reference solution
             the reference flux capacity
             a 2D list of parents (pop1, pop2)
    '''

    if not os.path.isfile(args.reference_model):
        logger.error("File not found {}".format(args.reference_model))

    with open(args.reference_model, "rb") as f:
        original_model = pickle.load(f)
    ref_sol = original_model.Solution
    reference_flux_capacity = get_total_flux_capacity(ref_sol)

    parent_pop = []
    for pickle_file in os.listdir(args.parent_directory):
        # Skip reference model
        if pickle_file in args.reference_model:
            continue
        fileName, fileExtension = os.path.splitext(pickle_file)
        if fileExtension != ".pickle":
            continue
        #Regular model
        else:
            model_file = os.path.join(args.parent_directory, pickle_file)
            with open(model_file, "rb") as f:
                model = pickle.load(f)

                if len(parent_pop) <= int(args.parents)-1:
                    model.population = None
                    parent_pop.append(model)
                else:
                    break

    return original_model, ref_sol, reference_flux_capacity, parent_pop


def f1GenerateMPI(mpi, ref_model, ref_solution, ref_id, parents, ngen, selfing,
                  samePop, crossing_population=False):

    f1_local = []

    for n in range(0, mpi.local.nhybrid):
        id = mpi.local.trialID(n)
        mpi.local.randomSet(id)
        xnew = generate_F1(ref_model, ref_solution, ref_id,
                           parents, 1, ngen, selfing, samePop,
                           crossing_population)
        xnew[0].id_seed = id
        f1_local.append(xnew[0])

    return f1_local


def calculate_equal_bins(x, nbin):
    '''Calculate bins of equal size'''
    npt = len(x)
    return np.interp(np.linspace(0, npt, nbin + 1),
                     np.arange(npt),
                     np.sort(x))

def pickBestMPI(mpi, f1_local, nRequired, ref_model,
                ref_solution, ref_id, parents, myGeneration, selection_method):
    '''
     From a local list of hybrids f1_local supplied on each rank
     return the globally agreed list of the best individuals.
     The same list (of length nRequired) is returned on all ranks.
    '''
    # Local sort (we need at most nlocal)
    nlocal = min(mpi.local.nhybrid, nRequired)
    #best_local = pickTheBestLocal(f1_local, nlocal, selection_method)
    if selection_method == 'random':
        # value1 and value2 will not be used
        value1 = attrgetter('solution.f')
        value2 = attrgetter('total_flux_capacity')
        # best_local = random.sample(f1_local, nlocal)
    elif selection_method == 'F-TF':
        value1 = attrgetter('solution.f')
        value2 = attrgetter('total_flux_capacity')
    elif selection_method == 'F-TFb':
        value1 = attrgetter('solution.f')
        value2 = attrgetter('total_flux_capacity')
    elif selection_method == 'TF-Fb':
        value2 = attrgetter('solution.f')
        value1 = attrgetter('total_flux_capacity')
    elif selection_method == 'F':
        value1 = value2 = attrgetter('solution.f')
    elif selection_method == 'TF-TC':
        value1 = attrgetter('total_flux_capacity')
        value2 = attrgetter('total_constraint_capacity')
    elif selection_method == 'TC-TF':
        value2 = attrgetter('total_flux_capacity')
        value1 = attrgetter('total_constraint_capacity')
    elif selection_method == 'GD':
        value1 = value2 = attrgetter('parental_genetic_distance')
    elif selection_method == 'F-GDb':
        value1 = attrgetter('solution.f')
        value2 = attrgetter('parental_genetic_distance')
    elif selection_method == 'GD-Fb':
        value2 = attrgetter('solution.f')
        value1 = attrgetter('parental_genetic_distance')

    # Record the scores "solution.f" and "total_flux_capacity"
    # Communicate the best nlocal scores
    # update: we don't use local sorting anymore, all the score are communicated
    # and sorting is done an all the results
    idlocal   = [0 for n in range(0, nlocal)]
    obj1local = [0 for n in range(0, nlocal)]
    obj2local = [0 for n in range(0, nlocal)]

    for n in range(nlocal):
        idlocal[n] = f1_local[n].id_seed
        obj1local[n] = value1(f1_local[n])
        obj2local[n] = value2(f1_local[n])

    idglobal = mpi.comm.allgather(idlocal)
    obj1global = mpi.comm.allgather(obj1local)
    obj2global = mpi.comm.allgather(obj2local)

    # Flatten [[], [], ..] -> [..]
    idglobal = [item for sublist in idglobal for item in sublist]
    obj1global = [item for sublist in obj1global for item in sublist]
    obj2global = [item for sublist in obj2global for item in sublist]

    #sortFitnessGlobal(idglobal, obj1global, obj2global)
    tup = zip(idglobal, obj1global, obj2global)
    if selection_method == 'F-TF':
        tupsort = sorted(tup, key = lambda x: (x[1], -x[2]), reverse = True)
    elif selection_method in ['F-TFb', 'F-GDb', 'TF-Fb' , 'GD-Fb']:
        min1, max1 = min(obj1global), max(obj1global)
        nbins = 4.0 # split in 4 bins of equal size and get the top 25%
        digitized = np.digitize(obj1global,
                                calculate_equal_bins(obj1global, nbins)) # put data in the bins
        if selection_method == 'TF-Fb':
            filtered = [x for x, i in zip(tup, digitized) if i == min(digitized)] # first bin
            the_rest = [x for x, i in zip(tup, digitized) if i != min(digitized)]
        else:
            filtered = [x for x, i in zip(tup, digitized) if i >= max(digitized)-1] # last bin
            the_rest = [x for x, i in zip(tup, digitized) if i < max(digitized)]
        if selection_method == 'F-TFb': # Smallest total flux
            tupsort = sorted(filtered, key = lambda x: -x[2], reverse=True)
        else:
            tupsort = sorted(filtered, key = lambda x: x[2], reverse=True)
        tupsort += the_rest

    elif selection_method == 'random':
        tupsort = tup
        random.shuffle(tupsort)
    else:
        tupsort = sorted(tup, key = lambda x: (x[1], x[2]), reverse = True)

    for n in range(0, len(idglobal)):
        idglobal[n]  = tupsort[n][0]
        obj1global[n] = tupsort[n][1]
        obj2global[n] = tupsort[n][2]

    # All ranks generate hybrids missing from top list
    # Non-optimal local hybrids are discarded
    newParents = []

    for n in range(0, nRequired):
        id = idglobal[n]
        # Do we have this individual? If not, regenerate from id
        present = False
        for m in range(0, nlocal):
            if (id == f1_local[m].id_seed):
                newParents.append(f1_local[m])
                present = True
                break

        if (not present):
            mpi.local.randomSet(id)
            xnew = generate_F1(ref_model, ref_solution, ref_id,
                               parents, 1, myGeneration, allow_selfing=False,
                               allow_samePop=True)
                               # allow_samePop= True because this is called during seleciton pressure
            xnew[0].id_seed = id
            newParents.append(xnew[0])

    return newParents


def pickSampleMPI(mpi, ref_model, ref_solution, ref_id, parents, myGeneration,
                  nRequired, population=False, same_pop=True):
    '''
    Deprecated: interbreeding is now done in a separate script.
     On all ranks, pick a sample at random, then generate the appropriate
     individuals
    '''
    # Generate the same sample on all ranks

    id = mpi.local.iterationNumber*mpi.local.nglobal
    mpi.local.randomSet(id)
    pickIndex = random.sample([n for n in range(0, mpi.local.nglobal)],
                              nRequired)

    # Form global population (all ranks)
    pick = []
    mpi.local.iterationNumber += 1

    for n in range(0, nRequired):
        id = mpi.local.iterationNumber*mpi.local.nglobal + pickIndex[n]
        mpi.local.randomSet(id)
        xnew = generate_F1(ref_model, ref_solution, ref_id,
                           parents, 1, myGeneration, allow_selfing=False,
                           allow_samePop=same_pop,
                           crossing_population=population)
        xnew[0].id_seed = id
        pick.append(xnew[0])

    return pick

def printPopulationSummary(mpi, pop, args):

    if mpi.root:
        for hybrid in pop:
            logger.info(("[id %i] \tF: %1.8f \tTF: %1.4f \tTC: %1.4f"
                         "\tGD: %1.4f\tP: %s\tG: %s") % (hybrid.id_seed,
                                          get_biomass_flux(hybrid.solution,
                                                           args.biomass_reaction_id),
                                          hybrid.total_flux_capacity,
                                          hybrid.total_constraint_capacity,
                                          hybrid.parental_genetic_distance,
                                          str(hybrid.population),
                                          str(hybrid.generation)))


def main(argv):
    # Constants
    biomass_reaction_id = default_biomass_reaction

    # MPI environment
    mpi = MPIHelper.MPIHelper(mpi4py.MPI.COMM_WORLD)

    mpi.info("\tRunning on %i MPI task(s)", mpi.size)
    t0 = time.time()

    args = get_args()
    assert args.selection_method in selection_methods_avail

    DEBUG = args.debug
    if DEBUG:
        logger.info("Debug mode ON!")

    # Random seed to come from user input ultimately
    mpi.local.masterSeed = int(args.random_seed)
    random.seed(mpi.local.masterSeed)

    collection_size = int(args.size) # Number of selected individuals
    t_hybrids   = int(args.t_hybrids)

    start_gen = int(args.start_gen) # start gen number (default = 1)
    NGen = start_gen+int(args.n_gen) -1 # number of generations

    mpi.local.set(t_hybrids)

    mpi.info('#' * 80)
    mpi.info("Parameters:")
    mpi.info("\tMaster RNG seed is: %d", mpi.local.masterSeed)
    mpi.info("\tGeneration starting at: %i", start_gen)
    mpi.info('\tEnding at: %i', NGen)
    mpi.info("\tSelection method: %s", args.selection_method)
    mpi.info('#' * 80)

    # All ranks
    # Load the initial collection of seeds etc
    start_time = time.time()

    mpi.info("Parent directory provided: loading the pickles")
    mpi.info("Loading reference model...")
    mpi.info("Loading reference solution...")
    original_model, ref_sol, reference_flux_capacity, parent_collection = \
        get_initial_parents_single(args)

    mpi.info("\tPopulations loaded in %1.3fs", time.time() - start_time)
    mpi.info("\nInitiate DB connection for data storage in file stub: %s",
             args.output_file)

    # DB I/O is distributed. Each rank writes to a separate data file
    # all of which can be consolidated in a post-processing step
    dbFileName = extract.MPIFileName(args.output_file, mpi.size, mpi.rank)

    storage = ZODB.FileStorage.FileStorage(dbFileName, create=True)
    db = ZODB.DB(storage)
    connection = db.open()
    dbroot = connection.root()

    popCount = 0
    t1 = time.time()

    # Store initial parent data in the db
    # Only one copy is required, written by root...
    parent_count = 0
    if mpi.root and save_initial_parents:
        for p in parent_collection:

            md = Model_Data(p,
                            store_reactions=True,
                            store_parents=False,
                            calc_zygosity=False)
            md.setSelected(True)
            dbroot[md.uuid] = md
            parent_count += 1
        mpi.info("\nSaving %i initial parents to the db", parent_count)

    # Start computation
    for gen in range(start_gen, NGen+1):
        mpi.local.iterationNumber += 1
        mpi.info("\nPopulation: %i \t Generation: %i", popCount+1, gen)
        start_time = time.time()

        f1_local = f1GenerateMPI(mpi, original_model, ref_sol,
                                 biomass_reaction_id,
                                 parent_collection,
                                 gen, False, True)
        mpi.info("\t%i models generated (per rank) in %1.3fs",
                 len(f1_local), time.time() - start_time)

        # 2 ) Apply selection pressure
        mpi.info("Selection of the %i best candidates...", collection_size)
        start_time = time.time()

        parent_collection = pickBestMPI(mpi, f1_local, collection_size,
                                        original_model, ref_sol,
                                        biomass_reaction_id,
                                        parent_collection, gen,
                                        args.selection_method)

        printPopulationSummary(mpi, parent_collection, args)
        mpi.info("\tSelection process took %1.3fs", time.time() - start_time)

        if args.save_all:
            start_time = time.time()
            local_sum_F = sum([f1.solution.f for f1 in f1_local])
            global_sum_F = mpi.comm.allreduce(local_sum_F, op=mpi4py.MPI.SUM)

            f1_count = len(f1_local)
            count = mpi.comm.allreduce(f1_count, op=mpi4py.MPI.SUM)
            avg_pop_F = global_sum_F / count

            mpi.info("\tAverage parental F =  %1.6f" % avg_pop_F)
            mpi.comm.barrier()

            # Write the final data base entries
            for f1 in f1_local:
                md = Model_Data(f1,
                                store_reactions=True,
                                store_parents=False,
                                calc_zygosity=False,
                                max_average_parental_F=avg_pop_F)
                md.setSelected( f1 in parent_collection )
                dbroot[md.uuid] = md
            transaction.commit()

            mpi.info("\t%i records saved in %.3fs",
                     count, time.time() - start_time)

    tnow = time.time()
    mpi.info("\n")
    mpi.info("Total execution time:  %.3fs", tnow - t0)

    mpi.info("Finished normally\n")


if __name__ == "__main__":
    main(sys.argv[1:])

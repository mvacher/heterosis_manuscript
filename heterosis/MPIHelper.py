##############################################################################
#
#  MPIHelper.py
#
#  Some helper classes
#
##############################################################################

import random

class Decomposition(object):

    comm = None
    nglobal = 0

    masterSeed = 13
    iterationNumber = 0

    # Takes an MPI communicator object
    def __init__(self, comm):
        self.comm = comm
        self.nhybrid = 0

    ##########################################################################
    #
    #  Compute work distribution
    #
    #  At each step, we generate trial hybrids
    #     0 ... nglobal - 1
    #
    #  We will distribute these evenly between all ranks so that each
    #  rank is responsible for nhybrid local trials ranging from
    #
    #     first ... first + nhybrid - 1
    #
    #  We try to distribute any odd remainder away from root
    #
    ##########################################################################

    def set(self, nglobal):

        self.nglobal = nglobal

        self.nhybrid = round(nglobal / self.comm.size)
        remainder = nglobal % self.comm.size
        if self.comm.rank >= (self.comm.size - remainder):
            self.nhybrid += 1

        worklist = self.comm.allgather(self.nhybrid)

        self.first = 0
        for n in range(0, self.comm.rank):
            self.first += worklist[n]

    ##########################################################################
    #
    #  Print the details of the decomposition (all ranks)
    #  Collective in self.comm
    #
    ##########################################################################

    def printDecomposition(self):

        for n in range(0, self.comm.size):
            if self.comm.rank == n:
                n0 = self.first
                n1 = n0 + self.nhybrid - 1
                print("\tRank %3i has local hybrids %5i -> %5i (%5i)"
                    % (self.comm.rank, n0, n1, n1 - n0 + 1))
            self.comm.barrier()

    ##########################################################################
    #
    #  Set random number generator for a local index 0 ... nhybrid
    #  This uniquely determines the trial id (which is returned).
    #
    ##########################################################################

    def trialID(self, indexLocal):

        id = self.iterationNumber*self.nglobal + self.first + indexLocal
        return id

    def randomSet(self, id):
        random.seed(self.masterSeed + id)
        # jumpahead not available in python 3.x
        # random.seed(self.masterSeed)
        # random.jumpahead(1 + id)


class MPIHelper(object):
    # MPI communicator information with default initialisation
    # The master random number generator seed must be the same on all ranks

    comm = None
    size = 1

    # work sharing

    def __init__(self, comm):
        self.comm = comm
        self.size = comm.size
        self.rank = comm.rank
        self.root = (self.rank == 0)
        self.local = Decomposition(comm)

    def info(self, *args):

        if (self.root):
            print(args[0] % (args[1:]))

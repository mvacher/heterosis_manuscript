import copy
import random
import re
import sys
from bisect import bisect
from copy import deepcopy
from operator import attrgetter
import uuid
import numpy as np

try:
    from cobra.flux_analysis.parsimonious import pfba
except: # cobra < 0.6
    from cobra.flux_analysis.parsimonious import optimize_minimal_flux as pfba

default_biomass_reaction = 'I_BIOMASS_F2'
DEBUG = False
parsimonious = True

def SBML2COBRAid(sbmlid):
    if sbmlid.startswith('R_'):
        return re.sub(r'^R_', '', sbmlid)
    elif sbmlid.startswith('C_'):
        return re.sub(r'^C_', '', sbmlid)
    else:
        return sbmlid


def get_percent(value, percent):
    return (value * float(percent)) / 100.0


def calculate_net_efficiency(reaction, method=1):
    '''
    The net efficiency is calculated per reaction
    It corresponds to : the computed flux / enzyme capacity (upper bound)

    ARGS:
        - reaction: A Model_Data object
        - method:    Integer for the metho to use
    Return:
        - The calculated efficiency
    '''
    eff = None
    if method == 1:
        x = abs(reaction['x'])
        genCap = max([abs(reaction['lower_bound']),
                      abs(reaction['upper_bound'])])
        try:
            eff = x / genCap * 100.0
        except:
        # FIXME: systematically add 1.0 to avoid null division
            eff = (x+1.0)/(genCap + 1.0) * 100
        return eff
    elif method == 2:
        pass

def optimize_model(original_model,
                   biomass_reaction_id=default_biomass_reaction,
                   copy=False):
    '''   Reset solution and optimize the model '''
    if copy:
        # Make a copy of the original model
        cobra_model = deepcopy(original_model)
    else:
        cobra_model = original_model

    # Reset solution:
    # cobra_model.solution = None

    # Reset all the obj coef in the model
    [setattr(x, 'objective_coefficient', 0.) for x in cobra_model.reactions]

    obj_value = 1.0
    objective_reaction = \
        cobra_model.reactions.get_by_id(SBML2COBRAid(biomass_reaction_id))
    objective_reaction.objective_coefficient = obj_value

    if parsimonious:
        try:
            cobra_model.solution = pfba(cobra_model)
        except:
            print("WARNING: unable to run parsimonious FBA.\
            Fallback to classic FBA")
            cobra_model.solution = cobra_model.optimize()
    else:
        cobra_model.solution = cobra_model.optimize()
    assert cobra_model.solution is not None

    return cobra_model.solution


def get_total_flux_capacity(model_solution):
    # REVIEW: should be replaced with: fba_solution.fluxes.abs().sum()
#     print("WARNING: get_total_flux_capacity is deprecated: use \
# 'fba_solution.fluxes.abs().sum()' instead.")
    fc = None
    try: # Old cobrapy version
        fc = sum([abs(flux) for rId, flux in model_solution.fluxes.items()])
    except:
        pass#print("Unable to calculate flux capacity")
    if fc is None:
        try:
            fc = model_solution.fluxes.abs().sum()
        except:
            print("Unable to calculate flux capacity")
    return fc


def get_total_constraint_capacity(model):
    fList = [abs(reaction.lower_bound + reaction.upper_bound)
             for reaction in model.reactions]
    return sum(fList)

def get_biomass_flux(solution, biomass_reaction_id):
    '''Get biomass flux for Legacy Solution'''
    return solution[SBML2COBRAid(biomass_reaction_id)]

def calculate_gen_var(list_of_parents,
                      normalized=False,
                      reference_solution=None):
    assert len(list_of_parents) == 2  # Need to have 2 parents in the list

    p1, p2 = list_of_parents
    if len(p1.reactions) != len(p2.reactions):
        raise ValueError("Parents reactions mismatch - {} vs {}\
            ".format((len(p1.reactions), len(p2.reactions))))
        return np.nan

    if type(p1.reactions) is dict:
        rList = p1.reactions.keys()
        p1_r = np.array([abs(p1.reactions[rid]['lower_bound'] +\
                             p1.reactions[rid]['upper_bound'])
                         for rid in rList])
        # Make sure loop in the same reaction order
        p2_r = np.array([abs(p2.reactions[rid]['lower_bound'] +
                        p2.reactions[rid]['upper_bound'])
                        for rid in rList])
    else:
        rList = [r.id for r in p1.reactions ]#if (hasattr(r, 'alleles') and r.alleles)]
        assert len(rList) > 0
        p1_r = np.array([abs(p1.reactions.get_by_id(rid).lower_bound
                        + p1.reactions.get_by_id(rid).upper_bound)
                        for rid in rList])
        p2_r = np.array([abs(p2.reactions.get_by_id(rid).lower_bound
                        + p2.reactions.get_by_id(rid).upper_bound)
                        for rid in rList])

    if normalized:
        assert reference_solution is not None
        ref = np.array([abs(reference_solution.fluxes[rid])
                        for rid in rList])
    try:
        if normalized:
            with np.errstate(invalid='ignore', divide='ignore'):
                ssd_list = (((p1_r - p2_r) ** 2) / ref)
            ssd = np.nansum(ssd_list)
        else:
            ssd = np.nansum(((p1_r - p2_r) ** 2))
    except:
        print("Warning: parents 1 and 2 have different number of reactions \
        {} vs {}. Cannot calculate SSD".format((len(p1_r), len(p2_r))))
        ssd = np.nan
    return ssd


def pickTheBest(hybrid_collection,
                n,
                by='solution.objective_value',
                thenby='total_flux_capacity',
                verbose=False):
    value1 = attrgetter(by)
    value2 = attrgetter(thenby)
    best = sorted(hybrid_collection,
                  key=lambda x: (value1(x), -value2(x)),
                  reverse=True)

    best = best[:n]

    if verbose:
        for hybrid in best:
            print('\t- %s\tF: %1.4f \tTF: %1.4f' %
                  (hybrid.name, getattr(hybrid, by), getattr(hybrid, thenby)))

    assert len(best) == n
    return best


def generate_parent_model_linear(original_model,
                                 biomass_reaction_id,
                                 reference_solution,
                                 lower_limit,
                                 upper_limit,
                                 allele_ids,
                                 homozyguous=False):
    ''' Generate a parent model from a linear model
        The constraints are based on the stoichiometric coef.
        of each element of the biomass reaction '''
    parent_model = copy.deepcopy(original_model)
    parent_model.uuid = random.getrandbits(64)
    # we use getrandbits to be able to regenare the same id twice (with the same seed) we use #uuid.uuid4()
    parent_model.id = parent_model.uuid

    biomass = parent_model.reactions.get_by_id(SBML2COBRAid(biomass_reaction_id))
    dummy_rxn_prefix = "dummy_rxn_"

    div = random.randint(1, 10)
    constraints = {}
    for metabolite in biomass.reactants:

        s = abs(biomass.get_coefficient(metabolite.id))
        rId = dummy_rxn_prefix + metabolite.id
        rFlux = reference_solution.fluxes[rId]

        generated_constraints = []  # Store the constraints
        for i in range(len(allele_ids)):
            ratio = random.uniform(lower_limit, upper_limit)  # 0.1,1.0#random.uniform(0.25, 1.50)
            gen_con = (s / div) * ratio  # /2.0 allow to shift the rfd lower
            generated_constraints.append(gen_con)  # Calculate the constraint

        constraints[rId] = {
            'ratio': ratio,
            'ref_flux': rFlux,
            'generated_constraints': generated_constraints
        }  # Store in the dict

        if DEBUG: print("%s\tOriginal Flux:\t%1.3f\tNew Constraint:\t%s\tRatio: %1.3f" % (
        rId, rFlux, str(generated_constraints), ratio))

    if DEBUG:
        print("DEBUG: Generating parent model - Tref: %1.3f\tVS\tTp: %1.3f" % (check_Tref_Tp[0], check_Tref_Tp[1]))

    # 3 - Apply constraint on the parent model
    for rId, constraint_data in constraints.iteritems():

        # Biomass should not be there (skipped in previous loop)
        if rId == biomass_reaction_id:
            print("Error: the biomass reaction should not be constrained !")
            sys.exit(1)

            # Get the reaction to constrain
        reaction_to_constrain = parent_model.reactions.get_by_id(SBML2COBRAid(rId))

        reaction_to_constrain.alleles = []
        for a in range(len(allele_ids)):
            # for constraint in constraint_data['generated_constraints']:
            constraint = constraint_data['generated_constraints'][a]
            allele_id = allele_ids[a]

            b = {"allele_id": allele_id,
                 'uuid': random.getrandbits(64),  # uuid.uuid4(),
                 "upper_bound": None,
                 "lower_bound": None}
            '''the genotype of the network, to a first assumption,
            controls the maximum activity possible for a particular reaction .
            The minimum activity should always be zero, whatever the genotype. '''
            if reaction_to_constrain.reversibility:
                if constraint > 0:
                    b["upper_bound"] = constraint
                    b["lower_bound"] = 0.0
                else:
                    b["lower_bound"] = constraint
                    b["upper_bound"] = 0.0
            else:
                b["upper_bound"] = constraint
                b["lower_bound"] = 0.0

            reaction_to_constrain.alleles.append(b)

        if homozyguous:
            b = reaction_to_constrain.alleles[0]
            for old_bound in reaction_to_constrain.alleles:
                old_bound = b

        # Set the effective constraint on the reaction
        reaction_to_constrain = apply_effective_constraint(reaction_to_constrain)

        if reaction_to_constrain.lower_bound > reaction_to_constrain.upper_bound:
            t = reaction_to_constrain.lower_bound
            reaction_to_constrain.lower_bound = reaction_to_constrain.upper_bound
            reaction_to_constrain.upper_bound = t
            print('''Error: LB >= UB
                reaction rID: %s
                original flux: %1.6f
                ratio: %1.3f
                constraint: %1.3f
                UB: %1.3f\tLB: %1.3f''' % (rId,
                                           constraint_data['ref_flux'],
                                           constraint_data['ratio'],
                                           constraint,
                                           reaction_to_constrain.upper_bound,
                                           reaction_to_constrain.lower_bound))
            sys.exit(0)

    return parent_model

def normalize_constraints(model, reference_flux_capacity, reference_sol=False, FVA=False):
    '''
    Normalize effective constraint (reactions' lower and upper bounds) against
    the reference flux distribution. This is done such as total constraint never
    exceeds the total reference flux distribution.
    '''
    constraints = []
    to_constrain = [] # keep track of reaction on which to apply the constraints
    # First pass collect the  raw constraint
    for r in model.reactions:
        if not hasattr(r, 'alleles'):
            continue
        if not r.alleles:
            continue
        # Depecrated:
        # if r.objective_coefficient != 0.0:
        #     continue
        # if r.boundary == 'system_boundary':
        #     continue

        if r.reversibility:
            assert r.upper_bound == abs(r.lower_bound)
            constraints.append(r.upper_bound)
        else:
            assert r.lower_bound == 0.0
            constraints.append(r.upper_bound )
        to_constrain.append(r.id)
    x = float(reference_flux_capacity) / float(sum([abs(c) for c in constraints]))
    normed_constraints = [c * x for c in constraints]

    assert len(constraints) == len(normed_constraints)

    i = 0
    # 2nd pass Normalization
    for rId in to_constrain:
        r = model.reactions.get_by_id(rId)
        assert r.alleles
        assert normed_constraints[i] > 0.0
        if r.reversibility:
            r.upper_bound = normed_constraints[i]
            r.lower_bound = -normed_constraints[i]
        else:
            assert r.lower_bound == 0.0
            r.upper_bound = normed_constraints[i]

        rev = 0
        if r.reversibility:
            rev=1

        i += 1

    assert i == len(constraints), "{} vs {}".format(i, len(constraints))


def generate_parent_model(original_model,
                          biomass_reaction_id,
                          reference_solution,
                          lower_limit,  # This define the range where the random number is picked
                          upper_limit,
                          allele_ids,
                          homozyguous=False):
    ''' Generate a parent model (adding constraint) from a reference solution '''

    parent_model = copy.deepcopy(original_model)
    parent_model.uuid =  random.getrandbits(64)  #
    # 1 - Generate a vector of constraint (C) such as:
    #     Tref <= Tparent
    #     Tref[i] * 0.25 < Tparent[i] < Tref[i] * 1.50
    div = random.randint(1, 10)
    constraints = {}
    for rId, rFlux in reference_solution.fluxes.iteritems():
        # Do not constraint the biomass reaction !
        if rId == biomass_reaction_id: continue
        # Do not constraint boundary reaction:
        if parent_model.reactions.get_by_id(rId).boundary == 'system_boundary':
            continue

        # We only constraint reaction carrying flux in the WT
        lock = False
        if abs(rFlux) <= 0.000001: lock = True

        generated_constraints = []  # Store the constraints
        for i in range(len(allele_ids)):
            ratio = random.uniform(lower_limit, upper_limit)  # 0.1,1.0#random.uniform(0.25, 1.50)
            gen_con = (rFlux / div) * ratio  # /2.0 allow to shift the rfd lower
            if lock: gen_con = 0.0
            generated_constraints.append(gen_con)  # Calculate the constraint

        # For easier debugging we store all the data (old, Flux, ratio, calculated_constrained)
        constraints[rId] = {
            'ratio': ratio,
            'ref_flux': rFlux,
            'generated_constraints': generated_constraints
        }  # Store in the dict

    # 3 - Apply constraint on the parent model
    for rId, constraint_data in constraints.iteritems():

        # Biomass should not be there (skipped in previous loop)
        if rId == biomass_reaction_id:
            print("Error: the biomass reaction should not be constrained !")
            sys.exit(1)

            # Get the reaction to constrain
        reaction_to_constrain = parent_model.reactions.get_by_id(SBML2COBRAid(rId))

        reaction_to_constrain.alleles = []
        for a in range(len(allele_ids)):
            constraint = constraint_data['generated_constraints'][a]
            allele_id = allele_ids[a]

            b = {"allele_id": allele_id,
                 'uuid': random.getrandbits(64),  # uuid.uuid4(),
                 "upper_bound": None,
                 "lower_bound": None}
            '''We assume that the genotype (constraint)
            controls the maximum activity possible for a particular reaction.
            The minimum activity should always be zero, whatever the genotype. '''
            if reaction_to_constrain.reversibility:
                if constraint > 0:
                    b["upper_bound"] = constraint
                    b["lower_bound"] = 0.0
                else:
                    b["lower_bound"] = constraint
                    b["upper_bound"] = 0.0
            else:
                b["upper_bound"] = constraint
                b["lower_bound"] = 0.0

            reaction_to_constrain.alleles.append(b)

        if homozyguous:
            b = reaction_to_constrain.alleles[0]
            for old_bound in reaction_to_constrain.alleles:
                old_bound = b

        # Set the effective constraint on the reaction
        reaction_to_constrain = apply_effective_constraint(reaction_to_constrain)

        if reaction_to_constrain.lower_bound > reaction_to_constrain.upper_bound:
            print('''Error: LB >= UB
                reaction rID: %s
                original flux: %1.6f
                ratio: %1.3f
                constraint: %1.3f
                UB: %1.3f\tLB: %1.3f''' % (rId,
                                           constraint_data['ref_flux'],
                                           constraint_data['ratio'],
                                           constraint,
                                           reaction_to_constrain.upper_bound,
                                           reaction_to_constrain.lower_bound))
            sys.exit(0)

    return parent_model


def generate_parent_model_fva(original_model,
                              biomass_reaction_id,
                              reference_solution,
                              fva,
                              lower_limit,
                              upper_limit,
                              allele_ids,
                              homozyguous=False):
    ''' Generate a parent model (adding constraint) from a reference solution '''
    parent_model = copy.deepcopy(original_model)
    parent_model.uuid =  random.getrandbits(64)  #

    constraints = {}
    for rId, rFlux in reference_solution.fluxes.items():
        # Do not constraint the biomass reaction !
        if rId == biomass_reaction_id: continue
        if fva.loc[rId]['minimum'] <= 1e-10 and fva.loc[rId]['maximum'] <= 1e-10:
            continue
        # Do not constraint boundary reaction:
        if parent_model.reactions.get_by_id(rId).boundary == 'system_boundary':
            continue

        generated_constraints = []  # Store the constraints
        for i in range(len(allele_ids)):
            #gen_con = 0.0 #Make sure the constraint is not null

            gen_con = random.uniform(fva.loc[rId]['minimum'],
                                     fva.loc[rId]['maximum'])

            generated_constraints.append(gen_con)  # Calculate the constraint

        # For easier debugging we store all the data (old, Flux, ratio, calculated_constrained)
        constraints[rId] = {
            'ref_flux': rFlux,
            'generated_constraints': generated_constraints
        }  # Store in the dict

    # 3 - Apply constraint on the parent model
    for rId, constraint_data in constraints.items():

        # Get the reaction to constrain
        reaction_to_constrain = parent_model.reactions.get_by_id(SBML2COBRAid(rId))

        reaction_to_constrain.alleles = []
        for a in range(len(allele_ids)):
            constraint = constraint_data['generated_constraints'][a]
            allele_id = allele_ids[a]

            b = {"allele_id": allele_id,
                 'uuid': random.getrandbits(64),  # uuid.uuid4(),
                 "upper_bound": None,
                 "lower_bound": None}
            '''We assume that the genotype (constraint)
            controls the maximum activity possible for a particular reaction.
            The minimum activity should always be zero, whatever the genotype. '''
            # Update now, we treat constraint as enzyme capacity
            if reaction_to_constrain.reversibility:
                if constraint > 0:
                    b["upper_bound"] = constraint
                    b["lower_bound"] = -constraint
                else:
                    b["lower_bound"] = constraint
                    b["upper_bound"] = -constraint
            else:
                b["upper_bound"] = constraint
                b["lower_bound"] = 0.0

            reaction_to_constrain.alleles.append(b)

        if homozyguous:
            # Turn the 2 raw constraints to the same values
            b = reaction_to_constrain.alleles[0]
            for old_bound in reaction_to_constrain.alleles:
                old_bound = b

        # Set the effective constraint on the reaction
        reaction_to_constrain = apply_effective_constraint(reaction_to_constrain)

        assert reaction_to_constrain.lower_bound < reaction_to_constrain.upper_bound

    return parent_model


def apply_effective_constraint(reaction_to_constrain, method='additive'):
    if method == 'additive':
        # Get the average of the 2 alleles as effective constraints
        if reaction_to_constrain.alleles:
            if reaction_to_constrain.reversibility:
                assert sum([ b['upper_bound']==abs(b['lower_bound'])
                for b in reaction_to_constrain.alleles]) == len(reaction_to_constrain.alleles)
                reaction_to_constrain.upper_bound = np.mean([b['upper_bound']
                                                             for b in reaction_to_constrain.alleles])
                reaction_to_constrain.lower_bound = -reaction_to_constrain.upper_bound
            else:
                assert sum([ (b['lower_bound']==0 or b['lower_bound']==0)
                for b in reaction_to_constrain.alleles]) == len(reaction_to_constrain.alleles)
                reaction_to_constrain.lower_bound = np.mean([b['lower_bound']
                                                             for b in reaction_to_constrain.alleles])
                reaction_to_constrain.upper_bound = np.mean([b['upper_bound']
                                                             for b in reaction_to_constrain.alleles])
                assert reaction_to_constrain.lower_bound ==0 or reaction_to_constrain.upper_bound==0
        else:  # these reactions do not have constraints
            pass
    elif method == 'random':
        # Pick up one bound randomly to use a effective constraint
        b = random.choice(reaction_to_constrain.alleles)
        reaction_to_constrain.lower_bound = b['lower_bound']
        reaction_to_constrain.upper_bound = b['upper_bound']

    elif method == 'dominance' or method == 'overdominance':
        # Pick the maximum
        LB = 0.0#-1000.0
        UB = 0.0#1000.0
        for b in reaction_to_constrain.alleles:
            if b['upper_bound'] > 0.0:
                if b['upper_bound'] > UB:
                    UB = b['upper_bound']
                    LB = 0.0
            else:
                if b['lower_bound'] < LB:
                    UB = 0.0
                    LB = b['lower_bound']

        if method == 'dominance':
            reaction_to_constrain.lower_bound = LB
            reaction_to_constrain.upper_bound = UB
        else:  # overdominance
            coeff = 0.05  # 5%
            if UB > 0.0:
                reaction_to_constrain.lower_bound = LB
                reaction_to_constrain.upper_bound = UB + (UB * coeff)
            else:
                reaction_to_constrain.lower_bound = LB - (LB * coeff)
                reaction_to_constrain.upper_bound = UB

    else:
        print('Error: Unknown method :%s.' % method)
        sys.exit(0)

    if reaction_to_constrain.lower_bound > reaction_to_constrain.upper_bound:
        print('''Failed to apply constraints : LB > UB
        rID: %s
        Method: %s\t
        LB\t\tUB\treversibility
        %1.6f\t%1.6f\t%s''' % (reaction_to_constrain.id, method,
                               reaction_to_constrain.lower_bound,
                               reaction_to_constrain.upper_bound,
                               reaction_to_constrain.reversibility))
        exit(0)
    return reaction_to_constrain


def quantify_heterosis(hybrid_model, p1, p2):
    ''' Degree of heterosis = biomass hybrid / biomass better parent'''
    if hasattr(hybrid_model, 'solution'):
        return hybrid_model.solution.objective_value / max([p1.solution.objective_value,
                                                            p2.solution.objective_value])
    elif hasattr(hybrid_model, 'F'):
        return hybrid_model.F / max([p1.F, p2.F])


def quantify_heterosis_mean(hybrid_model, p1, p2):
    ''' Degree of heterosis = biomass hybrid / biomass better parent'''
    if hasattr(hybrid_model, 'solution'):
        return hybrid_model.olution.objective_value / np.mean([p1.solution.objective_value,
                                                                p2.solution.objective_value])
    elif hasattr(hybrid_model, 'F'):
        return hybrid_model.F / np.mean([p1.F, p2.F])


def is_heterotic_hybrids(hybrid_model, p1, p2):
    ''' Return True if hybrid is a better parent'''
    if hasattr(hybrid_model, 'solution'):
        return hybrid_model.solution.objective_value > max([p1.solution.objective_value,
                                                            p2.solution.objective_value])
    elif hasattr(hybrid_model, 'F'):
        return hybrid_model.F > max([p1.F, p2.F])


def get_hybrid_type(model, p1, p2):
    # if Model_Data object
    if hasattr(model, 'F'):
        F_c = model.F
        F_p1 = p1.F
        F_p2 = p2.F
    elif hasattr(model, 'solution'):
        F_c = model.solution.objective_value
        F_p1 = p1.solution.objective_value
        F_p2 = p2.solution.objective_value
    else:
        print("Error: wrong object type")
        sys.exit()

    if F_c > F_p2 and F_c > F_p1:
        type = 'Above parent'
    elif F_c < F_p2 and F_c < F_p1:
        type = 'Below parent'
    else:
        type = 'Mid-parent'

    return type


def weighted_choice(choices):
    # "Example: weighted_choice([("WHITE",90), ("RED",8), ("GREEN",2)])"
    values, weights = zip(*choices)
    total = 0
    cum_weights = []
    for w in weights:
        total += w
        cum_weights.append(total)
    x = random.random() * total
    i = bisect(cum_weights, x)
    return values[i]


def cross_parents_genetic_models(original_model,
                                 reference_solution,
                                 biomass_reaction_id,
                                 parent_A_model,
                                 parent_B_model,
                                 weighted_gen_models):

    cross_model = copy.deepcopy(original_model)
    cross_model.uuid = random.getrandbits(64)  # uuid.uuid4()

    for rId, rFlux in reference_solution.fluxes.iteritems():
        # Do not constraint the biomass reaction !
        if rId == biomass_reaction_id: continue
        # Do not constraint boundary reaction:
        if cross_model.reactions.get_by_id(rId).boundary == 'system_boundary':
            continue

        reaction_to_constrain = cross_model.reactions.get_by_id(SBML2COBRAid(rId))
        reaction_to_constrain.alleles = []

        # Make sure reactions have the same number of allele
        assert len(parent_A_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles) == len(
            parent_B_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles)

        # Get a random alleles from the parents
        n_alleles = len(parent_A_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles)
        if n_alleles == 1:  # monoploid:
            parents = [random.choice([parent_A_model, parent_B_model])]

        elif n_alleles == 2:
            parents = [parent_A_model, parent_B_model]
        elif n_alleles == 4 or n_alleles == 6 or n_alleles == 8:  # Diploid (common)
            parents = [parent_A_model, parent_B_model] * (n_alleles / 2)

        else:
            print("Error: unknown number of alleles %i" % n_alleles)
            sys.exit()

        for parent in parents:
            b = random.choice(parent.reactions.get_by_id(SBML2COBRAid(rId)).alleles)
            reaction_to_constrain.alleles.append(b)

        # Set effective constaint as the average of the 2 allels
        effective_constraint_method = weighted_choice(weighted_gen_models)
        apply_effective_constraint(reaction_to_constrain, method=effective_constraint_method)

    return cross_model

def cross_models(original_model,
            #    reference_solution,
                biomass_reaction_id,
                parent_A, parent_B,
                method="random",
                effective_constraint_method='additive'):
    ''' updated function for cobra > 0.6. Replace cross_parents function '''
    cross_model = copy.deepcopy(original_model)
    cross_model.uuid =  random.getrandbits(64)

    rIds = [r.id for r in parent_A.reactions]
    for rId in rIds:
        reaction_to_constrain = cross_model.reactions.get_by_id(rId)
        reaction_to_constrain.alleles = []

        # Do not constraint the biomass reaction !
        if rId == biomass_reaction_id: continue

        pA_r = parent_A.reactions.get_by_id(rId)
        pB_r = parent_B.reactions.get_by_id(rId)

        if not pA_r.alleles or not pB_r.alleles:
            continue
        # Do not constraint boundary reaction:
        if cross_model.reactions.get_by_id(rId).boundary == 'system_boundary':
            continue

        # Make sure reactions have the same number of allele
        assert len(parent_A.reactions.get_by_id(rId).alleles) == len(
            parent_B.reactions.get_by_id(rId).alleles)

        # Get a random alleles from the parents
        if method == "random":
            for parent in [parent_A, parent_B]:
                b = random.choice(parent.reactions.get_by_id(rId).alleles)
                reaction_to_constrain.alleles.append(b)

        apply_effective_constraint(reaction_to_constrain,
                                   method=effective_constraint_method)
    return cross_model


def cross_parents(original_model,
                  reference_solution,
                  biomass_reaction_id,
                  parent_A_model,
                  parent_B_model,
                  method="random",
                  effective_constraint_method='additive',
                  weights = False): # Weight replaces cross_parents_genetic_models. if true, it overides effective_constraint_method and select an eff using weighted_choice
    # REVIEW: This is a legacy version to cross two model.
    # This function is replaced by cross_model()
    cross_model = copy.deepcopy(original_model)
    cross_model.uuid =  random.getrandbits(64)  #

    for rId, rFlux in reference_solution.fluxes.iteritems():
        # Do not constraint the biomass reaction !
        if rId == biomass_reaction_id: continue

        pA_r = parent_A_model.reactions.get_by_id(SBML2COBRAid(rId))
        pB_r = parent_B_model.reactions.get_by_id(SBML2COBRAid(rId))
        if not hasattr(pA_r,'alleles') or not hasattr(pB_r,'alleles'):
            continue
        if not pA_r.alleles or not pB_r.alleles:
            continue
        # Do not constraint boundary reaction:
        if cross_model.reactions.get_by_id(rId).boundary == 'system_boundary':
            continue

        # # We only constraint reaction carrying flux
        # if abs(rFlux) <= 0.000001: continue

        reaction_to_constrain = cross_model.reactions.get_by_id(SBML2COBRAid(rId))
        reaction_to_constrain.alleles = []

        # Make sure reactions have the same number of allele
        assert len(parent_A_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles) == len(
            parent_B_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles)

        # Get a random alleles from the parents
        if method == "random":
            n_alleles = len(parent_A_model.reactions.get_by_id(SBML2COBRAid(rId)).alleles)
            if n_alleles == 1:  # monoploid:
                parents = [random.choice([parent_A_model, parent_B_model])]

            elif n_alleles == 2:
                parents = [parent_A_model, parent_B_model]
            elif n_alleles == 4 or n_alleles == 6 or n_alleles == 8:  # Diploid (common)
                parents = [parent_A_model, parent_B_model] * (n_alleles / 2)

            else:
                print("Error: unknown number of alleles %i" % n_alleles)
                sys.exit()

            for parent in parents:
                b = random.choice(parent.reactions.get_by_id(SBML2COBRAid(rId)).alleles)
                reaction_to_constrain.alleles.append(b)

        elif method == "ecc":  # Deprecated:
            # Use the parent constraints to generate the hybrid ones:
            for parent in [parent_A_model, parent_B_model]:
                parent_reaction = parent.reactions.get_by_id(SBML2COBRAid(rId))
                # This methods create a new type of allele (so we have to select an id from the parents)
                parent_bound = random.choice(parent_reaction.alleles)
                b = {
                    'allele_id': parent_bound['allele_id'],
                    'upper_bound': parent_reaction.upper_bound,
                    'lower_bound': parent_reaction.lower_bound
                }
                reaction_to_constrain.alleles.append(b)

        if weights:
            effective_constraint_method = weights[rId]#weighted_choice(weights)
        # Set effective constaint as the average of the 2 allels
        apply_effective_constraint(reaction_to_constrain, method=effective_constraint_method)

    return cross_model


def get_combinations(parent_collection,
                     n_crosses,
                     allow_selfing=False,
                     allow_samePop=False  # allow two individual from the same population
                     ):
    '''
    generate a list of potential Crosses from a list of parent
    :param parent_collection: list of parents
    :param n_crosses: number of final crosses
    :param allow_selfing: allow self-polication
    :param allow_samePop: allow crossing within the same population
    :return: list of tuples (crosses)
    '''
    combinations = []
    # Randomly pick up 2 parent in the collection
    for i in range(n_crosses):
        paired = False
        while not paired:
            parent_A_model = random.choice(parent_collection)
            parent_B_model = random.choice(parent_collection)
            if allow_selfing:
                paired = True

            else:
                if allow_samePop and parent_A_model != parent_B_model:
                    paired = True
                elif not allow_samePop and parent_A_model.population != parent_B_model.population:
                    paired = True  # avoid self polination

        combinations.append((parent_A_model, parent_B_model))

    return combinations


def generate_F1(original_model, ref_sol,
                biomass_reaction_id,
                parent_collection,
                t_hybrids,
                generation,
                allow_selfing=False,
                allow_samePop=False,  # allow two individual from the same population,
                crossing_population=False , # The name of the population for the new cross (e.g :F1)
                effective_constraint_weights = False
                ):
    ''' Take a collection of parental inbred line
    cross the models and generate a collection of hybrids
    The new hybrid contains additional attributes such as the total capacity
    total fluxcapacity, the parents, etc...'''
    hybrid_collection = []

    # Randomly pick up 2 parent in the collection
    for i in range(t_hybrids):
        paired = False
        while not paired:
            parent_A_model = random.choice(parent_collection)
            parent_B_model = random.choice(parent_collection)

            if allow_selfing:
                paired = True

            else:
                if allow_samePop:
                    if parent_A_model != parent_B_model:
                        paired = True
                else:
                    if parent_A_model.population != parent_B_model.population:
                        paired = True  # avoid self polination
        cross_model = cross_parents(original_model,
                                    ref_sol,
                                    biomass_reaction_id,
                                    parent_A_model,
                                    parent_B_model,
                                    weights=effective_constraint_weights)

        # p2 is always the bigger parent
        if parent_A_model.solution.objective_value < parent_B_model.solution.objective_value:
            cross_model.p1 = parent_A_model
            cross_model.p2 = parent_B_model
        else:
            cross_model.p2 = parent_A_model
            cross_model.p1 = parent_B_model

        cross_model.name = "G_%i" % generation  # "x".join([p.name.replace("x","-") for p in [parent_A_model,parent_B_model]])
        cross_model.generation = generation
        if allow_selfing:
            cross_model.population = cross_model.p1.population
        elif allow_samePop:
            cross_model.population = cross_model.p1.population
        else:
            cross_model.population = None

        # Put this in a separate 'if' statement to override the previous ones.
        if crossing_population:
            cross_model.population = crossing_population

        # Normalization
        normalize_constraints(cross_model, get_total_flux_capacity(ref_sol))
        # Optimize
        optimize_model(cross_model, biomass_reaction_id)
        cross_model.total_flux_capacity = get_total_flux_capacity(cross_model.solution)
        cross_model.total_constraint_capacity = get_total_constraint_capacity(cross_model)
        cross_model.parental_genetic_distance = calculate_gen_var([cross_model.p1, cross_model.p2], False, ref_sol)

        hybrid_collection.append(cross_model)
    return hybrid_collection

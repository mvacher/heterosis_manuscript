import os
import pickle

def enum(*sequential, **named):
    """Handy way to fake an enumerated type in Python
    http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python
    """
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

def get_pickles_from_directory(directory):
    files = []
    for file in os.listdir(directory):
        fileName, fileExtension = os.path.splitext(file)
        if fileExtension == ".pickle":
            files.append(file)
    return files

def get_files_for_generation(directory, generation):
    '''
    Return all the files (pickle) corresponding to a given generation
    '''
    files = []
    for file in get_pickles_from_directory(directory):
        fileName, fileExtension = os.path.splitext(file)
        if fileName.startswith("G_"+str(generation)+"_"):
            files.append(file)
    return files

def get_max_generation_from_fn(directory):
    '''
    Return the max generation number for a population directory (inbred line)
    '''
    max = 0
    for file in get_pickles_from_directory(directory):
        fileName, fileExtension = os.path.splitext(file)
        if fileName.startswith("00_"):
            continue
        gen = int(fileName.split("_")[1])
        if gen > max:
            max = gen
    return max

def models_from_pickles(directory,files, size):
    models = []
    for pickle_file in files:

        with open(os.path.join(directory,pickle_file), "rb") as f:
            models.append(pickle.load(f))
        if len(models) == size:
            return models

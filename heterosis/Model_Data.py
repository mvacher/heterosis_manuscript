from persistent import Persistent
from copy import deepcopy
from heterosis.core import default_biomass_reaction, optimize_model


class Object(object):
    pass

class Model_Data(Persistent):

    def __init__(self,
                 model=None,
                 store_reactions=True,
                 store_parents=False,
                 calc_zygosity=False,
                 max_average_parental_F=None):

        self.uuid = None
        self.parents_uuid = []
        self.name = None
        self.generation = 0  # initial population of parents
        self.population = None
        self.total_flux = None  # total flux capacity
        self.total_constraint = None

        self.reactions = {}

        self.homozyguous = 0
        self.F = None
        self.max_average_parental_F = max_average_parental_F
        self.method = None
        self.Q = None  # degree of heterosis
        self.Q1 = None  # degree of heterosis with avg(parental population)_

        if type(model) is self.__class__:
            self.load_data_from_zope_md(model)

        else:
            self.load_data_from_model(model,
                                      store_reactions,
                                      store_parents,
                                      calc_zygosity,
                                      max_average_parental_F)

    def load_data_from_zope_md(self, md):
        self.uuid = md.uuid
        self.parents_uuid = md.parents_uuid
        self.name = md.name
        self.generation = md.generation
        self.population = md.population
        self.total_flux = md.total_flux
        self.total_constraint = md.total_constraint

        self.reactions = md.reactions

        self.homozyguous = md.homozyguous
        self.F = md.F

        self.method = md.method
        self.Q = md.Q
        self.Q1 = md.Q1

    def to_json(self):
        d = {'uuid': self.uuid,
             'parents_uuid': self.parents_uuid,
             'name': self.name,
             'generation': self.generation,
             'population': self.population,
             'total_flux': self.total_flux,
             'total_constraint': self.total_constraint,
             'reactions': self.reactions,
             'homozyguous': self.homozyguous,
             'F': self.F,
             'method': self.method,
             'Q': self.Q,
             'Q1': self.Q1}
        return d

    def load_data_from_model(self,
                             model,
                             store_reactions,
                             store_parents,
                             calc_zygosity,
                             max_average_parental_F):
        if not hasattr(model,'uuid'):
            from pprint import pprint
            pprint(dir(model))
            sys.exit()
        self.name = model.name
        self.uuid = model.uuid

        self.total_flux = model.total_flux_capacity
        self.total_constraint = model.total_constraint_capacity

        self.F = model.solution.f

        if hasattr(model, 'population'):
            self.population = model.population

        if hasattr(model, 'generation'):
            self.generation = model.generation

        if hasattr(model, 'p1') and hasattr(model, 'p2'):
            if (model.p1 is None) or (model.p2 is None):
                if store_parents:
                    print("Unable to store parents for model: {} (no parent)\
                    ".format(model.uuid))
            else:
                if store_parents:
                    self.p1 = Model_Data(model.p1)
                    self.p2 = Model_Data(model.p2)
                self.parents_uuid = [model.p1.uuid, model.p2.uuid]
                self.Q = model.solution.f / max([model.p1.solution.f,
                                                 model.p2.solution.f])
                if max_average_parental_F is not None:
                    self.Q1 = model.solution.f / max_average_parental_F

        # Deal with genetic models
        if hasattr(model, 'method'):
            self.method = model.method

        if store_reactions or calc_zygosity:
            for r in model.reactions:
                if hasattr(r, 'alleles'):
                    b = r.alleles
                else:
                    b = False

                if calc_zygosity and b:
                    if b[0]['allele_id'] == b[1]['allele_id']:
                        self.homozyguous += 1
                if store_reactions:
                    self.reactions[r.id] = {'x': r.x,
                                            'alleles': b,
                                            "lower_bound": r.lower_bound,
                                            'upper_bound': r.upper_bound
                                            }

    def get_percent_homozyguous(self):
        total = 0

        for rId, rDict in self.reactions.items():
            if rDict['alleles']:
                total += 1
                if rDict['alleles'][0]['uuid'] == rDict['alleles'][1]['uuid']:
                    self.homozyguous += 1

        percent = (float(self.homozyguous) / float(total))*100.0
        return percent

    def load_to_model(self, reference_model, parents=[],
                      homozyguous=False,
                      effective_constraint_method='additive',
                      sanity_check=True):

        model = deepcopy(reference_model)
        model.name = self.name
        model.uuid = self.uuid

        model.total_flux_capacity = self.total_flux
        model.total_constraint_capacity = self.total_constraint

        model.solution = Object()
        model.solution.f = self.F
        # try:
        #     model.solution.f = self.F
        # except:
        #     pass
        # if self.F is None:
        #     try :
        model.population = self.population
        model.generation = self.generation

        if self.parents_uuid:
            model.parents_uuid = self.parents_uuid

        model.Q = self.Q
        model.Q1 = self.Q1

        if self.reactions:
            for r in model.reactions:
                assert r.id in self.reactions

                model.reactions.get_by_id(r.id).alleles = \
                    self.reactions[r.id]['alleles']
                model.reactions.get_by_id(r.id).lower_bound = \
                    self.reactions[r.id]['lower_bound']
                model.reactions.get_by_id(r.id).upper_bound = \
                    self.reactions[r.id]['upper_bound']

                # Duplicate the genes if homozyguous
                if homozyguous:
                    model.reactions.get_by_id(r.id).alleles[1] = \
                        model.reactions.get_by_id(r.id).alleles[0]

        else:
            print("This Model Data instance has no reactions,"
                  "can't apply constraint")

        if sanity_check:
            s = optimize_model(model, default_biomass_reaction, copy=True)
            deltaF = abs(s.f-self.F)
            threshold = 0.1e-6
            if deltaF > threshold:
                print("sanity check failed: model {} could not be loaded \
                (objectives differ) delta: {}".format(self.uuid, deltaF))
                return False

        return model

    def clean_zope_attr(self):
        self.clean_zope_attr

    def setSelected(self, s):
        self.selected = s

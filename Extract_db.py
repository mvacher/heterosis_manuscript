##############################################################################
#
#  extract.py
#
#  Recombine parallel output into a single file
#
#  If we have the files:
#     db-file-size-0000
#     db-file-size-0001
#     ...
#     db-file-size-size-1
#
#  where "size" is the original number of mpi tasks used for output,
#  recombine them into a single file:
#
#     db-file
#
#  with the same data content. The original files can be  deleted at
#  the end of the process.
#
#  This code is serial.
#
##############################################################################

import os
import pickle
import sys

from heterosis.Model_Data import *
import ZODB.FileStorage
import ZODB.DB
import transaction
from copy import deepcopy
import argparse


def get_args():
    descr = 'Recombine parallel output into a single file'
    parser = argparse.ArgumentParser(description=descr)
    parser.add_argument('-i', '--stub', help='Stub name', required=True)
    parser.add_argument('-n', '--number_of_files',
                        help='Usually correspond to the number of MPI tasks',
                        required=True)
    parser.add_argument('-t', '--export_type',
                        help="'pickle' or 'db'", required=True)
    parser.add_argument('-o', '--output_file',default='db',  help='Output file',
                        required=True)
    parser.add_argument('-d', '--debug', help='debug mode (verbose)',
                        required=False,
                        action='store_true')
    return parser.parse_args()


def MPIFileName(stub, nfiles, rank):
    '''
    Generate a filename, e.g., stub-0001-0000
    '''
    filename = stub + "-%4.4i-%4.4i" % (nfiles, rank)
    return filename


def extract_data(stub,
                 nfiles,
                 export_type='db',
                 output_file=None,
                 verbose=True,
                 use_copy=True):
    '''
    #  Process parallel files
    '''
    if verbose:
        print("Processing %i files with stub %s" % (nfiles, stub))

    nrec = 0

    d = {}
    gen = {}
    for rank in range(0, nfiles):
        filename = MPIFileName(stub, nfiles, rank)
        if verbose:
            print("Current file: ", filename)

        storage = ZODB.FileStorage.FileStorage(filename, read_only=True)
        db = ZODB.DB(storage)
        connection = db.open()
        dbroot = connection.root()

        for key in dbroot.keys():
            zmd = dbroot[key]
            md = Model_Data(zmd)

            md.name = "M_{}_G_{}".format(nrec, md.generation)
            d[md.uuid] = md

            if md.generation not in gen:
                gen[md.generation] = 0

            gen[md.generation] += 1
            nrec += 1

        connection.close()
        db.close()

    storage_out = ZODB.FileStorage.FileStorage(output_file, create=True)
    db_out = ZODB.DB(storage_out)
    connection_out = db_out.open()
    dbroot_out = connection_out.root()

    for uuid, md in d.items():
        dbroot_out[uuid] = md
    transaction.commit()
    connection_out.close()
    storage_out.close()

    if verbose:
        print("Read a total of {} records".format(nrec))
        for g in sorted(gen.keys()):
            print("\tGeneration: {}, n = {}".format(g, gen[g]))

    # At this point, we store the "d" object using a standard
    # python pickle, e.g.:
    if export_type == "pickle":
        filename = stub + ".pickle"
        with open(filename, 'w') as pickleFile:
            pickle.dump(d, pickleFile)

    # Note that it appears to be problematic to write Persistant
    # objects from one ZODB to another owing to internal references
    # between database records . This is the reason for using a
    # simple pickle file.

    return

##############################################################################
#
#  Invoke via, e.g.,
#
#  python extract.py stub-name nfiles
#
##############################################################################

if __name__ == "__main__":
    args = get_args()
    extract_data(stub=args.stub,
                 nfiles=int(args.number_of_files),
                 export_type=args.export_type,
                 output_file=args.output_file)

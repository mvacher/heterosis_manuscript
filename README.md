
This repository contains codes and materials relating to the manuscript entitled 'Simulation of heterosis in a genome-scale metabolic network' by Michael Vacher and Ian Small


# Usage:

*Note:* Each script can be called with the --help switch to display the list of parameters available.

  * **Generate_initial_parents_population.py**: creates a collection of initial parents (seed models) with varying genetic background.
  * **Generate_inbred_lines_MPI.py**: simulates a selection pressure process over multiple generations. This script was designed to run on High-Performance Computer (HPC system). It uses MPI for parallel processing and should be called using mpirun (ie: mpirun -np number_of_cores python Generate_inbred_lines_MPI.py ...)
  * **Extract_db.py**: post-processing script to facilitate the concatenation of multiple databases created with 'Generate_inbred_lines_MPI.py'
  * **interbreeding_db.py**: simulate crosses between individuals from different inbred lines.

# System requirements:

To use the scripts in this repository, the machine should have the following installed:

  * Python >3.5
  * cobra==0.8.2
  * scikit-learn==0.18.1
  * scipy==0.19.0
  * seaborn==0.7.1
  * matplotlib==2.2.3
  * zc.lockfile==1.2.1
  * ZConfig==3.1.0
  * ZODB==5.2.0
  * zodbpickle==0.6.0
  * zope.interface==4.3.3
  * mpi4py==3.0.1
  * python-libsbml

The installation of these pre-requisites will vary depending on your host's operating system. The following sections contain detailed installation instructions that were tested on Ubuntu 16.04.

## Prerequisites installation on Ubuntu 16.04:
### 1) Installing the MPI library:
The Message Passing Interface Standard [(MPI)](https://en.wikipedia.org/wiki/Message_Passing_Interface) is a communication protocol for programming parallel applications. Simulations can be time-consuming depending on the parameters chosen (ie: number of generations, number of crosses to generate, etc.). 
Therefore MPI is use to run independent tasks in parallel and minimise time-to-solution. For population size >1000 we recommend using a parallel computing architectures.

  ```
  sudo apt-get install mpich
  ```

### 2) Installing Python:
  We recommend using [Anaconda](https://www.anaconda.com) in order to avoid conflict with an already existing installation of Python.
  The lastest version of Anaconda can be downloaded [here](https://www.anaconda.com/download/) and the installation instructions can be found [here](https://docs.anaconda.com/anaconda/install/linux/)

  Once installed, we strongly recommend creating a dedicated virtual environment.
  This will allow the installation of specific packages versions without altering the system installation.
  
  ```
  conda create -n your_env python=3.5.2
  conda activate your_env
  ```
  
  *Note:* 'your_env' can be changed to any name.

### 3) Installing libSBML:
[libSBML](http://sbml.org/Software/libSBML) is an open-source library for manipulating files encoded in the Systems Biology Markup Language (SBML) format.

  ```
  conda install -c SBMLTeam python-libsbml
  ```

## 4) Installing the required python packages:
  ```
  pip install -r requirements.txt
  ```



  After following this steps, the system should have all the libraries and packages required to run the scripts.
  
  If you require further assistance, please feel free to contact the authors.

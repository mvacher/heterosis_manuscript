import argparse
import pickle
import sys
import os
import cobra.io
import logging
from cobra.flux_analysis import flux_variability_analysis

from heterosis.core import *

# Logging
logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s %(name)s %(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger("Generate-initial-parents")


def main(argv):
    desc = '''
    This script generates a collection of initial parents.
    Example:
    python Generate_initial_parents_population.py -i my_model.xml -x biomass_reaction_id
    -p 500  -o initial_parents/ --fva
    '''
    genVar_help = '''Genetic variation of the initial population.
    Format: lower_limit,upper_limit
    Example: -g 0.35,1.15
    Default values = 0.2,1.2
    '''
    parser = argparse.ArgumentParser(description=desc,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-i', '--input_file', help='SBML file', required=True)
    parser.add_argument('-x', '--biomass_reaction_id',
                        help='SBML ID of the biomass reaction to use',
                        required=True)
    parser.add_argument('-p', '--parents', help='Number of initial parents',
                        required=True)
    parser.add_argument('-g', '--genvar', help=genVar_help)
    parser.add_argument('-o', '--output_directory', help='output directory',
                        required=True)
    parser.add_argument('--reference_model_filename',
                        help='Name of the reference model file (pickle)',
                        default='00_reference_model.pickle',
                        required=False)
    parser.add_argument('-z', '--homozyguous',
                        help='Create homozyguous individuals (default=False)',
                        required=False, action='store_true')
    parser.add_argument('-j', '--json', help='save as json (default=pickle)',
                        required=False, action='store_true')
    parser.add_argument('-d', '--debug', help='debug mode (verbose)',
                        required=False , action='store_true')
    parser.add_argument('-l', '--linear',
                        help='Create linear models (default=False)',
                        required=False, action='store_true')
    parser.add_argument('-f', '--fva', required=False, default=True,
                        action='store_true',
                        help='Pick allele values within FVA range')
    parser.add_argument('-r', '--fraction_of_optimum',
                        required=False,
                        default=.95,
                        type=float,
                        help='Define the fraction_of_optimum for the FVA \
                        (default=0.95)')
    args = parser.parse_args()

    sbml_file = args.input_file
    n_parents = int(args.parents)

    DEBUG = args.debug
    if DEBUG:
        logger.info("Debug mode ON!")

    logger.info('\tLoading SBML model from file: {}'.format(sbml_file))

    #Read the model only once:
    biomass_reaction_id = args.biomass_reaction_id #'R_Bio_opt'
    original_model = cobra.io.read_sbml_model(sbml_file)

    ref_sol = optimize_model(original_model, biomass_reaction_id, copy=True)
    reference_flux_capacity = get_total_flux_capacity(ref_sol)

    logger.info('''\nCreate Reference model and flux distribution.
                -F: %1.4f
                -total flux capacity: %1.4f
        ''' % (get_biomass_flux(ref_sol, biomass_reaction_id),
               reference_flux_capacity))

    if args.genvar:
        gv = args.genvar.split(',')
        if len(gv) != 2:
            logger.info("Error: Malformated genetic coefficient.",
                  "Must be like: -g 0.35,1.15.\nExit!")
            sys.exit()
        low_lim = float(gv[0])
        up_lim = float(gv[1])
    else:
        low_lim = 0.2
        up_lim = 1.2

    logger.info('''\nGenerating main parent collection:
                - N: %i
                - Lower_limit: %1.2f
                - Upper_limit: %1.2f
                - Homozyguous: %s''' % (n_parents, low_lim, up_lim,
                                         str(args.homozyguous)))

    if args.fva:
        logger.info("Running FVA: fraction_of_optimum=({})"\
                    .format(args.fraction_of_optimum))
        model = copy.deepcopy(original_model)
        [setattr(x, 'objective_coefficient', 0.) for x in model.reactions]
        obj_value = 1.0

        objective_reaction = model.reactions.get_by_id(SBML2COBRAid(biomass_reaction_id))
        objective_reaction.objective_coefficient = obj_value
        fva = flux_variability_analysis(model,
                                        fraction_of_optimum=args.fraction_of_optimum)

    n_created_parents = 0
    n_failed = 0
    while n_created_parents <= n_parents:
        if args.linear:
            pm = generate_parent_model_linear(original_model,
                                              biomass_reaction_id,
                                              ref_sol,
                                              low_lim,
                                              up_lim,
                                              ["A", "B"], args.homozyguous)
        elif args.fva:
            pm = generate_parent_model_fva(original_model,
                                           biomass_reaction_id,
                                           ref_sol,
                                           fva,
                                           low_lim,
                                           up_lim,
                                           ["A", "B"], args.homozyguous)
        else:
            pm = generate_parent_model(original_model,
                                       biomass_reaction_id,
                                       ref_sol,
                                       low_lim,
                                       up_lim,
                                       ["A", "B"], args.homozyguous)
        if not pm:
            logger.error("Error: failed to create parent model")
            n_failed += 1
            continue

        pm.name = "M_"+str(n_created_parents)      #alphabet[x]#
        pm.generation = 0 # Default generation value
        if args.debug:
            optimize_model(pm, biomass_reaction_id, copy=False)
            logger.info("Normalization: %s"%pm.name)
            logger.info("\tBefore:\tF: %1.4f\tTFC: %1.2f\tTCC:%1.2f"%\
                  (pm.solution.objective_value,
                   get_total_flux_capacity(pm.solution),
                   get_total_constraint_capacity(pm)))

            normalize_constraints(pm, reference_flux_capacity)
            optimize_model(pm, biomass_reaction_id, copy=False)
            logger.info("\tAfter:\tF: %1.4f\tTFC: %1.2f\tTCC:%1.2f"%\
                  (pm.solution.objective_value,
                   get_total_flux_capacity(pm.solution),
                   get_total_constraint_capacity(pm)))

        else:
            normalize_constraints(pm, reference_flux_capacity, ref_sol, fva)
            optimize_model(pm, biomass_reaction_id, copy=False)

        if pm.solution.objective_value <= 0.00000001:
            n_failed += 1
            continue

        pm.total_flux_capacity = get_total_flux_capacity(pm.solution)
        pm.total_constraint_capacity = get_total_constraint_capacity(pm)

        if args.json:
            cobra.io.save_json_model(pm,
                                     os.path.join(args.output_directory,
                                                  pm.name+'.json'))
        else:
            with open(os.path.join(args.output_directory,
                                   pm.name+'.pickle'), "wb") as f:
                pickle.dump(pm, f)

        n_created_parents += 1
        if n_created_parents % 10 == 0 and n_created_parents != 0:
            logger.info("\t- {} models created".format(n_created_parents))
    logger.info("\t- {} models saved ({} failed) ".format(n_created_parents,
                                                       n_failed))
    # save the reference model:
    rm = os.path.join(args.output_directory,
                      args.reference_model_filename)
    original_model.Solution = ref_sol
    with open(rm, "wb") as f:
        pickle.dump(original_model, f)

    logger.info("Reference model saved to: {} ".format(rm))


if __name__ == "__main__":
    main(sys.argv[1:])

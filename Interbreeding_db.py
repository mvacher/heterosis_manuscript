import argparse
import copy
import pickle
import time
from random import choice
from scipy.spatial.distance import pdist, squareform
import numpy as np
import logging

import transaction
from   ZODB import FileStorage, DB

from heterosis.core import *
from heterosis.Model_Data import *

# Logging
logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s %(name)s %(levelname)s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger("interbreeding_db")

def select_candidates(db_files, total_candidate=False,
                      generation=False, selection='random',
                      cutoff=0.00000001):
    c=[]
    if len(db_files) == 1:
        db_file = db_files[-1]

        storage = FileStorage.FileStorage(db_file)
        db = DB(storage)
        connection = db.open()
        root = connection.root()

        if selection == 'random':
            targets = [model for key, model in root.items()
                       if model.generation == generation and model.F > cutoff]
            logger.info("Random selection of {} models (total = {})".format(total_candidate,
                                                                             len(targets)))
            candidates = random.sample(targets, total_candidate)
            assert len(candidates) == total_candidate

        if selection == 'best':
            targets = [model for key, model in root.iteritems() if model.generation == generation]
            logger.info("Selection of {} best models (total = {}) ".format(total_candidate,
                                                                           len(targets)))
            candidates = pickTheBest(targets,total_candidate, by='F',
                                     thenby='total_flux', verbose=True)

            assert len(candidates) == total_candidate

        # For some reason it sometime failed if the object are not fully copied.
        c = [copy.deepcopy(model) for model in candidates]
        db.close()

    elif len(db_files) == 2:
        for idx, db_file in enumerate(db_files):
            while True:
                try:
                    storage = FileStorage.FileStorage(db_file)
                    db = DB(storage)
                    connection = db.open()
                    root = connection.root()
                except:
                    # print("Database %s cannot be accessed retrying in a few seconds"%db_file)
                    time.sleep(3)
                else:
                    break
            if selection == 'random':
                targets = [model for key, model in root.iteritems() if
                           model.generation == generation  and model.F > cutoff]
                logger.info(("Random selection of {} models (total = {}) -"
                            "\t Population: {}} ").format(total_candidate,
                                                          len(targets),
                                                          idx))
                candidates = random.sample(targets, total_candidate)
                assert len(candidates) == total_candidate
            elif selection == 'best':
                targets = [model for key, model in root.iteritems() if model.generation == generation]
                logger.info("Selection of {} best models (total = {}) ".format(total_candidate,
                                                                               len(targets)))
                candidates = pickTheBest(targets, total_candidate, by='F', thenby='total_flux', verbose=True)
                assert len(candidates) == total_candidate

            elif selection in ['all', 'closest', 'farthest']:
                candidates = [model for key, model in root.iteritems() if
                           model.generation == generation and model.F > cutoff]
                logger.info("Selecting ALL individuals ({}) population ({})".format(len(candidates), idx))

            else:
                logger.error("Unknown selection type: '{}'. Exit".format(selection))
                db.close()
                sys.exit()

            for model in candidates:
                m = copy.deepcopy(model)
                m.population=idx
                c.append(m)
            db.close()

    else:
        logger.error("Incorrect number of db files {} ".format(len(db_files)))
        logger.error(db_files)
        sys.exit(1)

    if selection in ['closest', 'farthest']:
        logger.info("Computing distance matrix between all the collected models")
        matrix = []
        reaction_list = None
        for model in c:
            # Create a reaction_list to maintain the a consistent reaction order
            if reaction_list is None:
                reaction_list = [r for r, rData in model.reactions.items()
                                 if rData['alleles']]
            row = [] # All the reaction value in a model
            for rId in reaction_list:
                r = model.reactions[rId]
                assert r['alleles']
                if r['upper_bound'] == 0.0 and r['lower_bound'] != 0.0:
                    row.append(r['lower_bound'])
                elif (-r['upper_bound']==r['lower_bound']) or (r['upper_bound'] != 0.0 and r['lower_bound'] == 0.0):
                    row.append(r['upper_bound'])
                else:
                    logger.error("Cannot process reaction data {}".format(r))
            matrix.append(row)
        #make sure the number of model is the same in the matrix
        assert len(matrix)==len(c)
        # Calculate distance matrix
        distmat = squareform(pdist(matrix))
        n_select = len(c)#total_candidate*2.0 # We take more, to make sure the final number is >= total_candidate
        if selection == 'closest':
            # Smallest values
            indices =  np.argpartition(distmat.flatten(), n_select)#[:n_select]
        else: #largest values
            indices =  np.argpartition(distmat.flatten(), -n_select)[::-1]#[-n_select:]
        potentials_parents = np.vstack(np.unravel_index(indices, distmat.shape)).T
        logger.info('\tNumber of potential combinations: {}'.format(len(potentials_parents)))
        logger.info("\tmin/max value: {}".format(distmat[potentials_parents[0][0]][potentials_parents[0][1]]))

        parents = []
        avoid_duplicate = {} #store selection model id to avoid picking the same model twice
        for i, coords in enumerate(potentials_parents):
            #print(coords,c[coords[0]].uuid, c[coords[1]].uuid, c[coords[0]].population, c[coords[1]].population)
            m1, m2 = c[coords[0]],c[coords[1]]
            if m1.uuid in avoid_duplicate or m2.uuid in avoid_duplicate:
                continue
            if m1.population != m2.population:
                for p in [m1,m2]:
                    avoid_duplicate[p.uuid]=True
                    parents.append(p)
                    print('\t- {}\tF: {} \tpop: {}\t d: {}'.format(p.uuid, p.name,
                            p.F, p.population, distmat[coords[0]][coords[1]]))
            if len(parents) >= total_candidate*2.0:
                return parents
        assert len(parents)!=0 ,"No parent found"
        logger.info("Warning: {} selected parents < {} (provided paramater)".format(len(parents),
                                                                                    total_candidate))
        return parents

    return c


if __name__ == "__main__":
    desc = '''
    Read a ZODB of ModelData object (generally inbreds) and map the constraints
    into cobra models. Then the models are crossed to generate the F1
    '''

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('-i', '--reference_dir',
                        help='Directory containing the reference model and solution',
                        required=True)
    parser.add_argument('-p', '--population_db',nargs='+',
                        help='ZODB file (1 or several)', required=True)
    parser.add_argument('-n', '--n_parents',
                        help='number of models to use as parents',
                        required=True)
    parser.add_argument('-g', '--parent_generation',
                        help='Generation (default = last generation available)',
                        default=False, required=False)
    parser.add_argument('-t', '--n_crossings',
                        help='Number of crossings to generate', required=True)
    parser.add_argument('-k', '--cross_type',
                        help='F1 or F2', default=False, required=True)
    parser.add_argument('-o', '--output_db',
                        help='ZODB to save the new population', required=True)
    parser.add_argument('-z', '--seed', help="Random seed (default =10)",
                        required=False, default=10)
    parser.add_argument('-d', '--debug', help='debug mode (verbose)',
                        required=False
                        , action='store_true')
    parser.add_argument( '--test_genetic_models',
                        help='simulate multiple genetic models',
                         default=False,
                         required=False,
                         action='store_true')  # store_true is useful for flag (no argument)
    parser.add_argument('-w', '--eff_weights',
                        help='Dictionary with effective constraint weights (pickle)',
                        required=False, default=False)
    parser.add_argument('--parent_selection', help='random or best',
                        required=False, default='random')
    args = parser.parse_args()

    if args.seed:
        random.seed(int(args.seed))
        logger.info("Setting random seed: %s" % (args.seed))
    if args.debug:
        DEBUG = True
        logger.info( "Debug mode ON!")

    biomass_reaction_id = "R_I_BIOMASS_F2"  # 'R_Bio_opt'

    logger.info("Loading original model from SBML file: %s" % args.reference_dir)
    with open(args.reference_dir+'/00_reference_model.pickle', "rb") as f:
        original_model = pickle.load(f)
    with open(args.reference_dir+'/00_reference_solution.pickle', "rb") as f:
        reference_solution = pickle.load(f)
    reference_flux_capacity = get_total_flux_capacity(reference_solution)


    start_time = time.time()

    logger.info("\nExtracting parent models (Model_Data) from DB")
    logger.info('\tSelection method: {}'.format(args.parent_selection))
    parents_md = select_candidates(args.population_db,
                                   total_candidate=int(args.n_parents),
                                   generation=int(args.parent_generation),
                                   selection=args.parent_selection)

    logger.info("-> %i models data loaded in %1.3fs" % (len(parents_md),
                                                       time.time() - start_time))

    logger.info("Converting parents MD to cobra models")
    start_time = time.time()
    parents = []
    for pmd in parents_md:
        m = pmd.load_to_model(original_model)
        if not m:
            continue
        parents.append(m)
    logger.info("-> %i parent models converted in %1.3fs" % (len(parents),
                                                            time.time() - start_time))
    #printPopulationSummary(parents)

    logger.info("Initialising db for storing new models %s" % args.output_db)
    storage = FileStorage.FileStorage(args.output_db, create=True)
    db = DB(storage)
    connection = db.open()
    root = connection.root()
    # save the parents
    for p in parents_md:
        p = Model_Data(p) #Attempts to reduce the size of the models
        root[p.uuid] = p
   #transaction.commit()
    logger.info("-> %i parents saved to db" % len(parents_md))

    logger.info( "Generating %i crosses - type: %s" % (int(args.n_crossings),
                                                       args.cross_type))
    allow_selfing = False
    allow_samePop = True
    if args.cross_type == 'F1':
        allow_samePop = False
        pop0 = np.mean([p.solution.f for p in parents if p.population == 0])
        pop1 = np.mean([p.solution.f for p in parents if p.population == 1])
        best_parental_pop_F = max([pop0, pop1])
    if allow_samePop:
        best_parental_pop_F = np.mean([p.solution.f for p in parents])

    genetic_models = []
    if args.test_genetic_models:
        genetic_models = ['dominance', 'additive', 'random','overdominance']
    else:
        genetic_models = ['additive']
    logger.info("\nCrosses will use the following genetic models:\n\t-%s" %
          "\n\t- ".join(genetic_models))

    effective_constraint_weights = False
    if args.eff_weights:
        logger.info("\n\tLoading weights from pickle: %s" % args.eff_weights)
        with open(args.eff_weights, "rb") as f:
            effective_constraint_weights = pickle.load(f)

    for m in genetic_models:
        start_time = time.time()
        for i in range(int(args.n_crossings)):
            paired = False
            while not paired:
                parent_A_model = choice(parents)
                parent_B_model = choice(parents)

                if allow_selfing:
                    paired = True

                else:
                    if allow_samePop:
                        if parent_A_model != parent_B_model:
                            paired = True
                    else:
                        if parent_A_model.population != parent_B_model.population:
                            paired = True  # avoid self polination

            cross_model = cross_parents(original_model,
                                        reference_solution,
                                        biomass_reaction_id,
                                        parent_A_model,
                                        parent_B_model,
                                        method="random",
                                        effective_constraint_method=m,
                                        weights = effective_constraint_weights)

            normalize_constraints(cross_model, reference_flux_capacity)

            optimize_model(cross_model, biomass_reaction_id)
            assert cross_model.solution.fluxes is not None

            cross_model.name = "x".join([p.name.replace("x", "-") for p in
                                         [parent_A_model, parent_B_model]])
            cross_model.method = 'additive'
            cross_model.p1 = parent_A_model
            cross_model.p2 = parent_B_model
            cross_model.generation = parent_A_model.generation + 1
            cross_model.population = args.cross_type
            cross_model.total_flux_capacity = get_total_flux_capacity(cross_model.solution)
            cross_model.total_constraint_capacity = get_total_constraint_capacity(cross_model)

            cross_model_md = Model_Data(cross_model,
                                        store_reactions=True,
                                        store_parents=False,
                                        max_average_parental_F=best_parental_pop_F)
            root[cross_model_md.uuid] = cross_model_md

            if i % 100 == 0:
                logger.info("\t- %i crosses generated" % i)

        transaction.commit()
        logger.info("Genetic model: %s finished in %1.3fs" % (m,
                                                              (time.time() - start_time)))
    db.close()
